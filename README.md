# T550-Ausleser

Landis+Gyr ULTRAHEAT T550 pythonscript zum Auslesen der Daten. Ich habe das Script von Sedelmaier.at (dankeschön!) so abgeändert, daß es auch mit python3 funktioniert. 

Ich habe dazu einen USB-Lesekopf verwendet, den ich bei [Ebay](https://www.ebay.de/itm/313460034498?_trkparms=amclksrc%3DITM%26aid%3D1110002%26algo%3DSPLICE.SOI%26ao%3D2%26asc%3D20200722115301%26meid%3D56f629450c894b7c841510a1ee2dce86%26pid%3D100044%26rk%3D1%26rkt%3D6%26sd%3D313884760667%26itm%3D313460034498%26pmt%3D0%26noa%3D1%26pg%3D2508447%26algv%3DSellersOtherItemsV2&_trksid=p2508447.c100044.m43681) gekauft habe. 

Unter Debian/Raspbian mußte ich folgendes vorbereiten:
- sudo apt install python3-serial apache2
- sudo adduser www-data dialout
- sudo a2enmod cgi
- git clone https://codeberg.org/HoSnoopy/T550-Ausleser
- cd T550-Ausleser
- sudo cp wz*py /usr/lib/cgi-bin/

Dazu habe ich 2 cgi-scripte geschrieben:
- wzaehler.py: gibt alles heraus, was der Zähler ausgibt
- wzkwh.py: gibt nur den aktuellen Zählerstand heraus

Abrufbar wäre das dann zB unter http(s)://rechnername/cgi-bin/wzkwh.py
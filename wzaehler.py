#!/usr/bin/python3
#coding: utf-8

from __future__ import print_function
import cgi, cgitb
import subprocess
import serial, time




print("Content-Type: text/html\n\n")
print("""
        <html><head><title>W&auml;rmez&auml;hlere-Status</title>
        </head>
        <body>
        
""")

ser = serial.Serial("/dev/ttyUSB0", baudrate=300, bytesize=7, parity="E", stopbits=1, timeout=2, xonxoff=0, rtscts=0)

#send init message
ser.write('\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'.encode())
ser.write('\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'.encode())

#send request message
ser.write("/?!\x0D\x0A".encode())
#ser.flush();
ser.flushInput()
ser.flushOutput()
time.sleep(.5)

#send read identification message
print(ser.readline())

#change baudrate
ser.baudrate=2400

try:
    #read data message
    while True:
        response = ser.readline().decode('utf-8')
        print(response)
        if response.find('!') >= 0:
            break
finally:
    ser.close()



print("</body></html>")
